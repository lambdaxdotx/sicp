#lang racket

(define (make-rat n d) (cons n d))
(define (numer z) 
  (let ((g (gcd n d)))
	(/ (car z) g))
(define (denom z)
  (let ((g (gcd n d)))
	(/ (cdr z) g))
