
const mult = (x, y) => x * y
const sub1 = x => x - 1

const fact = n =>
  (n === 0) ?
    1 :
    mult(n, fact(sub1(n)))

console.log( fact(5) )

const lift = t => n =>
  (n === 1) ?
    1 :
    mult(n, t(t)(sub1(n)))

console.log( lift(lift)(5) )

// const Y = f => (t => f(t(t)))(t => f(t(t)))    // strict version
// const Y = f => (t => f(t(t)))(t => f(x => t(t)(x)))
// const Z = f => (t => f(x => t(t)(x)))(t => f(x => t(t)(x)))
// inverse eta
const _Y = f => g => (t => f(x => t(t)(x)))(t => f(x => t(t)(x)))(g)
// Y = f => (t => t(t))(t => f(x => t(t)(x)))

const fact0 = t => n =>
  (n === 0) ?
    1 :
    mult(n, t(sub1(n)))

console.log( _Y(fact0)(5) )
// console.log( Z(fact0)(5) )
