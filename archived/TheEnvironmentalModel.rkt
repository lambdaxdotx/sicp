#lang racket

#|
;; add messages returning mutating methods
(define (cons x y)
  (lambda (msg)
    (cond ((eq? msg 'CAR )       x)
          ((eq? msg 'CDR )       y)
          ((eq? msg 'PAIR?)      #t)
          ((eq? msg 'SET-CAR!) (lambda (new-car) (set! x new-car))) ;; <---- returning a
          ((eq? msg 'SET-CDR!) (lambda (new-cdr) (set! y new-cdr))) ;; procedure object
          (else (error "pair cannot" msg)))))
(define (car p)           (p 'CAR))
(define (cdr p)           (p 'CDR))
(define (pair? p)         (p 'PAIR?))
(define (set-car! p new) ((p 'SET-CAR!) new))
(define (set-cdr! p new) ((p 'SET-CDR!) new))
|#

;; add private methods !
(define (cons x y)                                         ; private state variables
  (define change-car! (lambda (new-car) (set! x new-car))) ; private procedure
  (define change-cdr! (lambda (new-cdr) (set! y new-cdr))) ; private procedure
  (lambda (msg . args)
    (cond ((eq? msg 'CAR )       x)
          ((eq? msg 'CDR )       y)
          ((eq? msg 'PAIR?)      #t)
          ((eq? msg 'SET-CAR!) (change-car! (first args))) ;; <---- directly take care of
          ((eq? msg 'SET-CDR!) (change-cdr! (first args))) ;; the procedure-application
          (else (error "pair cannot" msg)))))
(define (car p)           (p 'CAR))
(define (cdr p)           (p 'CDR))            ; I don't have to remember what type
(define (pair? p)         (p 'PAIR?))          ; of value is returned by the object
(define (set-car! p new)  (p 'SET-CAR!  new))  ; (number vs procedure). Thus selectors
(define (set-cdr! p new)  (p 'SET-CDR!  new))  ; and mutators perfom in a uniform manner




(define foo (cons 1 2))
(car foo)
(cdr foo)
(pair? foo)
(set-car! foo 11)
(set-cdr! foo 0)
(car foo)
(cdr foo)
