#lang racket

(define (cons1 a b)
  (define (power x n)
    (if (= n 0) 1
        (* x (power x (- n 1)))))
  (* (power 2 a) (power 3 b)))

(define (car1 c)
  (if (= 0 (remainder c 2))
      (+ 1 (car1 (/ c 2)))
      0))

(define (cdr1 c)
  (if (= 0 (remainder c 3))
      (+ 1 (cdr1 (/ c 3)))
      0))

(cdr1 (cons1 7 31))
(car1 (cons1 7 31))
