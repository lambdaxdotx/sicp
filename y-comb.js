
const fact0 = t => n =>
  (n === 0) ?
    1 :
    n * t(n - 1)

const fib0 = t => n =>
  (n === 0) ?
    1 :
    (n === 1) ?
    1 :
    t(n - 1) + t(n - 2)
/*
 * Curry's lazy version
 *
 * Y = f => (x => f(x(x)))
 *         ((x => f(x(x))))
 *
 * Do an Alpha conversion:
 *
 * Y = f => (g => f(g(g)))
 *         ((g => f(g(g))))
 *
 * Recall the inverse eta transformation:
 *
 * t(t) ----> x => t(t)x
 *
 */

// Finally the strict version:
const Y = f => x => (g => f(x => g(g)(x)))
                   ((g => f(x => g(g)(x))))(x)

console.log(
  Y(fact0)(5)
)

for (let i = 0; i < 10; i++) {
  console.log(
    Y(fib0)(i)
  )
}
