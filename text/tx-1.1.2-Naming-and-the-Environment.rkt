#|
1.1.2 Naming and the Environment
|#
#lang racket
(require rackunit)
(define e 0.0000001)

(define size 2)

(check-eq?
  size 
  2
  )
(check-eq?
  (* 5 size)
  10
  )

(define pi 3.14159)
(define radius 10)

(check-=
  (* pi (* radius radius))
  314.159
  e
  )

(define circumference (* 2 pi radius))

(check-=
  circumference
  62.8318
  e
  )

#|
1. _**variable**_
2. _**value**_
3. _**environment**_ _**global environment**_
|#
