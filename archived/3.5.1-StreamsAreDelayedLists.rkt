#lang racket
(define nil `())

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (enumerate-interval low high)
  (cond ((> low high) nil)
        (else (cons low
                    (enumerate-interval (+ 1 low) high)))))

(define (prime? n)
  (define (find-divisor test-divisor)
    (define (square x) (* x x))
    (define (divides? a b) (= (remainder b a) 0))
    (cond ((> (square test-divisor) n) n)
          ((divides? test-divisor n) test-divisor)
          (else (find-divisor (+ test-divisor 1)))))
  (define smallest-divisor (find-divisor 2)) 
  (= n smallest-divisor))

(define (sum-primes a b)
  (accumulate +
              0
              (filter prime? (enumerate-interval a b))))

; compute the 2nd prime in the interval 10000 to 500000
;(car (cdr (filter prime? (enumerate-interval 1000 500000))))

(newline)

;; stream package
(define (memo-proc proc)
  (let ((already-run? false) (result false))
    (lambda ()
      (if (not already-run?)
          (begin (set! result (proc))
                 (set! already-run? true)
                 result)
          result))))
; 1) Constructors & Selectors
;(define (cons-stream a b)
;  (cons a (lambda () b))) ;; need special form !!
(define (force0 exp) (exp))
(define (stream-car s) (car s))
(define (stream-cdr s) (force0 (cdr s)))
(define the-empty-stream nil)
(define (stream-null? x) (null? x))

; 2) Test *:+*:+* Stream is List not pairs !!
;(stream-car (cons-stream 'a 'b))
;(stream-cdr (cons-stream 'a 'b))
;(define myStream (cons-stream -1 (cons-stream 3 the-empty-stream)))


(define (stream-ref s n)
  (if (= n 0)
      (stream-car s)
      (stream-ref (stream-cdr s) (- n 1))))
;(define (stream-map proc s)
;  (if (stream-null? s)
;      the-empty-stream
;      (cons-stream (proc (stream-car s))
;                   (stream-map proc (stream-cdr s)))))
(define (stream-for-each proc s)
  (if (stream-null? s)
      'done
      (begin (proc (stream-car s))
             (stream-for-each proc (stream-cdr s)))))

(define (display-line x)
  (newline)
  (display x))

(define (display-stream s)
  (stream-for-each display-line s))

(define (stream-filter pred s)
  (cond ((stream-null? s) the-empty-stream)
        ((pred (stream-car s))
         (cons (stream-car s)
               (lambda () (stream-filter pred (stream-cdr s))))) ; need special form
        (else (stream-filter pred (stream-cdr s)))))

;(display-stream myStream)
;(display-stream (stream-filter (lambda (x) (> x 0)) myStream))

; compute the 2nd prime in the interval 10000 to 500000
(define (stream-enumerate-interval low high)
  (cond ((> low high) the-empty-stream)
        (else (cons low
                    (lambda () (stream-enumerate-interval (+ 1 low) high))))))

(car (cdr (filter prime? (enumerate-interval 1000 500000)))) ;vs.
(stream-car (stream-cdr (stream-filter prime? (stream-enumerate-interval 1000 500000)))) ;vs.
