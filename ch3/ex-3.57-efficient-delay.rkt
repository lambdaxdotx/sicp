#lang racket
(require racket/stream)
; (stream? v) -> #t/#f
; (stream-empty? s) -> #t/#f
; empty-stream : the stream with no elements
; (stream-cons x y) -> a lazy stream
; (stream a b c ...) -> for nested stream-conses ending with empty-stream
; (stream-first s)
; (stream-rest s)
;
; (stream-ref s i) -> ith element
; (stream-append s ...)
; (stream-map f s)
; (stream-for-each f s)
; (stream-filter pred s)

#|
(define (stream-ref s n)
  (if (= n 0)
      (stream-car s)
      (stream-ref (stream-cdr s) (- n 1))))
|#


(define (stream-map2 proc . argstreams) 
  (if (stream-empty? (stream-first argstreams))
      empty-stream
      (stream-cons
       (apply proc (map stream-first argstreams))
       (apply stream-map2 (cons proc (map stream-rest argstreams))))))
(define (add-streams s1 s2) (stream-map2 + s1 s2)) 

;; Define streams implicitly
(define fibb 
  (stream-cons 0                        
               (stream-cons 1
                            (add-streams fibb (stream-rest fibb)))))
; 0 1 1 2 3 5 8 13 .......      fibs
;     0 1 1 2 3 5  .......      fibs
;     1 1 2 3 5 8  .......      (stream-rest fibs)
(newline)
(stream-ref fibb 0)
(stream-ref fibb 1)
(stream-ref fibb 2)
(stream-ref fibb 3)
(stream-ref fibb 4)
(stream-ref fibb 5)
(stream-ref fibb 6)
(stream-ref fibb 7)

