#lang racket
(require racket/trace)

(define (comb n m)
  (cond ((= m 0) 1)
        ((= m n) 1)
        (else (+ (comb (- n 1) (- m 1))
                 (comb (- n 1) m)))))

(display (comb 0 0))
(display "\n")
(display (comb 1 0))
(display (comb 1 1))
(display "\n")
(display (comb 2 0))
(display (comb 2 1))
(display (comb 2 2))
(display "\n")
(display (comb 3 0))
(display (comb 3 1))
(display (comb 3 2))
(display (comb 3 3))
(display "\n")
(display (comb 4 0))
(display (comb 4 1))
(display (comb 4 2))
(display (comb 4 3))
(display (comb 4 4))
(display "\n")

(trace comb)
(comb 5 2)


