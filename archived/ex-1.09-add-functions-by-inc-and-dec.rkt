#lang racket
(require racket/trace)
(define (inc x) (+ 1 x))
(define (dec x) (- x 1))

(define (plus0 a b)
  (if (= a 0) 
      b 
      (inc (plus0 (dec a) b))))

(define (plus1 a b)
  (if (= a 0) 
      b 
      (plus1 (dec a) (inc b))))

(trace plus0)
(trace plus1)

(plus0 4 5) ; The first process is recursive
(plus1 4 5) ; The second process is iterative
